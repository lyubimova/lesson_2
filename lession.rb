#!/user/bin/env ruby
class MegaGreeter
    attr_accessor :names

    #Create the new object
    def initialize (names = "World")
        @names = names
    end

    #Say hi to everbody
    def say_hi
        if @names.nil?
            puts "..."
        elsif @names.respond_to?("each")
            #@names is a list of some kind.iterate!
            @names.each do |name|
                puts "Hello #{name}!"
            end
        else
            puts "Hello #{@names}!"
        end
    end

    #Say bye to everybody

    def say_bye
        if @names.nil?
            puts "..."
        elsif @names.respond_to?("join")
            #Join the list element with commands
            puts "Goodbye #{@names.join(", ")}.Come back soon!"
        else
            puts "Goodbye #{@names}.Come back soon!"
        end
    end
end


    if __FILE__ == $0
        mq = MegaGreeter.new
        mq.say_hi
        mq.say_bye

        mq.names = "Zeke"
        mq.say_hi
        mq.say_bye

        mq.names = ["Albert", "Brenda", "Charles", "Dave"]
        mq.say_hi
        mq.say_bye


        mq.names = nil
        mq.say_hi
        mq.say_bye
    end

